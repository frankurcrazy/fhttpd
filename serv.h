#ifndef __SERV_H__
#define __SERV_H__
#include "http.h"

int worker();
int handle_request(struct request_headers* header, int sock);
#endif
