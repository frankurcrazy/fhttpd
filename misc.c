#include "misc.h"
#include <stdio.h>

char* trim(char* origin) {
    uint32_t str_len = 0;
    register int32_t i;

    str_len = strlen(origin);

    if (str_len == 0) {
        return origin;
    }

    /* remove tab, newline and spaces backward */
    for (i=str_len-1; i>=0; --i) {
        if (origin[i] == ' ' || origin[i] == '\t' || origin[i] == '\n' \
            || origin[i] == '\r' ) {
            origin[i] = '\0';
        } else {
            break;
        }
    }

    str_len = strlen(origin);
    if (str_len == 0) {
        return origin;
    }

    /* return the first non-space pointer */
    for (i=0; i<str_len; ++i) {
        if (origin[i] != ' ' && origin[i] != '\t' && origin[i] != '\n' \
            && origin[i] != '\r') {
            return &origin[i];
        }
    }
    
    return origin;
}

