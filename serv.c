#include "serv.h"
#include "conn.h"
#include "http.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <sys/param.h>
#include <sys/sendfile.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>

#define WORKER 4
#define BACKLOG 10
#define PORT 8080
#define HOST "0.0.0.0"
#define DOCUMENT_ROOT "./webroot"
#define INDEX_FILE "index.html"
#define SERVER_STRING "fuckhttpd/1.0"
#define BUFLEN 1024000

int main(int argc, char** argv) {
    pid_t pid;
    int msock = 0;
    int worker_counter = 0;
    int status;
    register int i = 0;

    msock = passiveTCP(HOST, PORT, BACKLOG);
    
    /* spawning up to WORKER workers */
    while (1) {
        if (worker_counter < WORKER) {
            pid = fork();
            if (pid > 0) {
                ++worker_counter;    
            } else if (pid < 0) {
                perror("fork()");
                continue;
            } else { 
                /* child */
                worker(msock);
                exit(EXIT_SUCCESS);
            }
        } else {
            wait(&status);
            if (WIFEXITED(status))
                fprintf(stderr, "Worker exited with status: %d\n", WEXITSTATUS(status));
        }
    }
}

int worker(int sock) {
    int ssock = 0;
    struct sockaddr_in client;
    int len = 0;
    char buf[BUFLEN];
    struct request_headers *header = NULL;

    fprintf(stderr, "Worker %d started.\n", getpid());
    while ((ssock = accept(sock, (struct sockaddr*) &client, &len)) > 0) {
        if (http_request_get_request(ssock, buf, BUFLEN) > 0) {
            header = http_request_new();
            http_request_parse(header, buf, strlen(buf));
            http_request_debug(header);
            
            handle_request(header, ssock);
         } else {
            goto end;
         }

        end:
        if (header) http_request_free(header);
        shutdown(ssock, SHUT_RDWR);
        close(ssock);
    }
}

int handle_request(struct request_headers* header, int sock) {
    char pathbuf[MAXPATHLEN];
    char path[MAXPATHLEN];
    char response[BUFLEN];
    char *error_404 = "HTTP/1.1 404 Not Found";
    char *error_500 = "HTTP/1.1 500 Internal Error";
    char *message_200 = "HTTP/1.1 200 OK";
    char *error_403 = "HTTP/1.1 403 Forbidden";
    int len = 0;
    time_t t;
    struct tm *now;
    char timestr[255];
    int filefd;
    ssize_t filesize = 0;
    ssize_t sendsize = 0;
    char *rpath = NULL;

    t = time(NULL);
    now = localtime(&t);
    strftime(timestr, 255, "%a, %d %b %G %H:%M:%S %Z" ,now);
    len = strlen(header->uri);
    
    if (header->uri[len-1] == '/') {
        snprintf(pathbuf,MAXPATHLEN, "%s/%s%s", DOCUMENT_ROOT, header->uri, INDEX_FILE);
    } else {
        snprintf(pathbuf,MAXPATHLEN, "%s/%s", DOCUMENT_ROOT, header->uri);
    }
   

    rpath = realpath(pathbuf, path);
    if (!rpath) {
        fprintf(stderr, "Requested: %s, not found\n", path);
        snprintf(response, BUFLEN, "%s\r\n" \
                                  "Server: %s\r\n" \
                                  "Date: %s\r\n" \
                                  "Content-Type: text/html\r\n" \
                                  "Content-length: 23\r\n" \
                                  "\r\n" \
                                  "<h1>404 Not found.</h1>", \
                                  error_404, SERVER_STRING, timestr
        );

        sendall(sock, response, strlen(response), 0);
        return 404;
    } else {
        if ( header->file_extension && !strcmp(header->file_extension, "cgi") ) {
            /* a cgi program */
            pid_t pid;
            int status;

            pid = vfork();
            if (pid > 0) {
                wait(&status);    
            } else if (pid == 0) {
                snprintf(response, BUFLEN, "%s\r\n", message_200);
                sendall(sock, response, strlen(response), 0); 
                if (header->query_string) {
                    setenv("QUERY_STRING", header->query_string, 1);
                } else setenv("QUERY_STRING", "", 1);

                dup2(sock, STDOUT_FILENO);
                dup2(sock, STDIN_FILENO);
                dup2(sock, STDERR_FILENO);
                execl(rpath, "cgi", NULL);
                exit(EXIT_FAILURE);
            } else {
                perror("vfork()");     
                goto error_500;
            }

            return 200;

            error_500:
            snprintf(response, BUFLEN, "%s\r\n" \
                                      "Server: %s\r\n" \
                                      "Date: %s\r\n" \
                                      "Content-Type: text/html\r\n" \
                                      "Content-length: 34\r\n" \
                                      "\r\n", \
                                      "<h1>500 Internal Server Error</h1>", \
                                      error_500, SERVER_STRING, timestr
            );
            sendall(sock, response, strlen(response), 0);
            return 500;


        } else {
            /* normal file */
            filefd = open(path, O_RDONLY); 
            if (filefd > 0) {
                /* get filesize */
                filesize =  lseek(filefd, 0, SEEK_END);
                lseek(filefd, 0, SEEK_SET); // rewind to beginning

                snprintf(response, BUFLEN, "%s\r\n" \
                                          "Server: %s\r\n" \
                                          "Date: %s\r\n" \
                                          "Content-Type: text/html\r\n" \
                                          "Content-length: %ld\r\n" \
                                          "\r\n", \
                                          message_200, SERVER_STRING, timestr, \
                                          filesize 
                );
                sendall(sock, response, strlen(response), 0);
                sendsize = sendfile(sock, filefd, NULL, filesize);
                fprintf(stderr, "Sent %s (%ld/%ld bytes)\n", path, sendsize, filesize);
                return 200;
            } else {
                if (errno == EACCES) {
                    fprintf(stderr, "Requested %s, permission denied.\n", path);
                    snprintf(response, BUFLEN, "%s\r\n" \
                             "Server: %s\r\nDate: %s\r\n" \
                             "Content-Type: text/html\r\n" \
                             "Content-length: 22\r\n" \
                             "\r\n" \
                             "<h1>403 Forbidden</h1>",
                             error_403, SERVER_STRING, timestr, \
                             filesize 
                    );
                    sendall(sock, response, strlen(response), 0);
                    return 403;
                }
            }
        }
    }
}

