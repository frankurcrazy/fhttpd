/*
   A simple HTTP request header parser
*/

#include "http.h"
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <stdio.h>
#include <sys/socket.h>


struct request_headers* http_request_new() {
    struct request_headers* request;

    request = (struct request_headers*) calloc(sizeof(struct request_headers), 1);
    if (!request) {
        perror("malloc()");
        return NULL;
    } else {
        return request;
    }
}

int http_request_free(struct request_headers* header) {
    assert(header);

    struct request_header *iter, *next;

    if (!header) {
        fprintf(stderr, "http_request_free():NULL header.\n");
        return -1;
    }

    if (header->uri) free(header->uri);

    /* free header entries */
    iter = header->entries;
    while (iter) {
        if (iter->key) free(iter->key);
        if (iter->value) free(iter->value);
        
        next = iter->next;
        free(iter);
        iter = next;
    }

    if (header) free(header);
    return 0;
}

/*
    Parse request in buf and save result to header
*/
int http_request_parse(struct request_headers* header, char* buf, int len) {
    assert(buf);

    char *bufdup = NULL;
    char *saveptr_header;
    char *saveptr_line;
    const char *delim_header = "\r\n";
    const char *delim_line = ":";
    const char *delim_firstline = " ";
    char *header_token, *line_token;
    char *get[4] = {"get", "GET"};
    char *post[5] = {"post", "POST"};
    char *head[5] = {"head", "HEAD"};
    register int i =0;
    int len_tmp;
    struct request_header* new_header;

    bufdup = strndup(buf, len); 
    if (!bufdup) {
        perror("strdup()");
        return -1;
    }

    /* get the first line from request */
    header_token = strtok_r(bufdup, delim_header, &saveptr_header);

    /* get the request method */
    line_token = strtok_r(header_token, delim_firstline, &saveptr_line);
    if ( !strcmp(line_token,get[0]) || !strcmp(line_token, get[1])) {
        header->method = GET;
    } else if (!strcmp(line_token, post[0]) || !strcmp(line_token, post[1])) {
        header->method = POST;
    } else if (!strcmp(line_token, head[0]) || !strcmp(line_token, head[1])) {
        header->method = HEAD;
    } else {
        header->method = UNKNOWN;
    }

    /* get the request uri */
    line_token = strtok_r(NULL, delim_firstline, &saveptr_line);
    if (!line_token) {
        fprintf(stderr, "http_request_parse: Cannot find uri.\n");
    } else {
        /* get uri and query string */
        header->uri = strndup(line_token, MAX_HEADER_LENGTH-1); 
        len_tmp = strlen(header->uri);

        /* get file extension */
        for (i=len_tmp; i>=0; --i) {
            if (header->uri[i] == '.') {
                break;
            }
        }
        if (header->uri[i] == '.') {
            if (i+1 < len_tmp) {
                header->file_extension = header->uri+i+1;    
            } else {
                header->file_extension = NULL;
            }
        } else {
            header->file_extension = NULL;
        }

        /* get query string */
        for (i=0; i<len_tmp; ++i) {
            if (header->uri[i] == '?') {
                header->uri[i] = '\0';

                if (i+1 < len_tmp) {
                    header->query_string = (header->uri)+i+1;
                } else {
                    header->query_string = NULL; 
                }

                break;
            }
        }
    }

    /* get each request header */
    while ( (header_token = strtok_r(NULL, delim_header, &saveptr_header)) != NULL) {
        /* GET KEY */
        line_token = strtok_r(header_token, delim_line, &saveptr_line); 
        if (line_token) {
            new_header = (struct request_header*) calloc(sizeof(struct request_header), 1);
            new_header->key = strndup(line_token, MAX_HEADER_LENGTH);

            /* GET VALUE */
            line_token = strtok_r(NULL, delim_line, &saveptr_line);
            while (*line_token == ' ') ++line_token;
            new_header->value = strndup(line_token, MAX_HEADER_LENGTH);

            if (header->entries != NULL) {
                new_header->next = header->entries;
                header->entries = new_header;
            } else {
                header->entries = new_header;
            }

        } else {
           // No key found 
        }
    }

    free(bufdup);
    return 1;
}

char* http_request_get(struct request_headers* header, char* key) {
    assert(key && header);  

    struct request_header* iter;

    if (!(key && header)) return NULL;

    if (!strcmp(key, "uri")) {
        return header->uri;
    } else if (!strcmp(key, "query_string")) {
        return header->query_string;
    }

    /* traverse through entries */
    iter = header->entries;
    while (iter) {
        if (!strcmp(iter->key, key)) {
            return iter->value;
        }

        iter = iter->next;
    }

    return NULL;
}

enum method http_request_get_method(struct request_headers* header) {
    return header->method;
}

void http_request_debug(struct request_headers* header) {
    struct request_header *iter;
    fprintf(stderr, "[DEBUG request header]\n");
    fprintf(stderr, "method: \"%s\"\n" \
                    "uri: \"%s\"\n" \
                    "ext: \"%s\"\n" \
                    "query_string: \"%s\"\n", \
                    (header->method==GET?"GET":header->method==POST?"POST": \
                     header->method == HEAD?"HEAD":"UNKNOWN"),
                    header->uri,header->file_extension, header->query_string
                    );
    iter = header->entries;
    while (iter) {
        fprintf(stderr, "%s: \"%s\"\n", iter->key, iter->value);
        iter = iter->next;
    }
    fprintf(stderr, "\n");
}

int http_request_get_request(int sock, char* buf, int len) {
    int bytes_recvd = 0;
    int total_len = 0;

    bzero(buf, len);
    
    while ((bytes_recvd = recv(sock, buf+total_len,len-total_len, 0)) > 0) {
        total_len += bytes_recvd;

        if (strstr(buf, "\r\n\r\n")) {
            break;
        }
    }

    if (bytes_recvd == 0) {
        fprintf(stderr, "Client disconnected prematurely.\n");
        return -1;

    } else if (bytes_recvd < 0) {
        perror("recv()");
        return -1;
    } else {
        return total_len;
    }

}

