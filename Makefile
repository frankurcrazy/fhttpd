serv: serv.c serv.h misc.o conn.o http.o
	$(CC) $(CFLAGS) -o serv serv.c conn.o misc.o http.o

conn.o: misc.o conn.c conn.h
	$(CC) $(CFLAGS) -c -o conn.o misc.o conn.c

misc.o: misc.c misc.h
	$(CC) $(CFLAGS) -c -o misc.o misc.c

http.o: http.c http.h
	$(CC) $(CFLAGS) -c -o http.o http.c

clean:
	rm -rf *.o *.out serv
