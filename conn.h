#ifndef __CONN_H__
#define __CONN_H__

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include <netdb.h>
#include <string.h>

int isDomain(char* host);
int connectTCP(char *host, uint16_t port);
int passiveTCP(char *host, uint16_t port, int backlog);
int sendall(int sock, void* buf, size_t len, int flags);
#endif

