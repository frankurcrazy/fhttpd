#ifndef __HTTP_H__
#define __HTTP_H__

#define MAX_HEADER_LENGTH 8192

enum method {
    GET,
    POST,
    HEAD,
    UNKNOWN
};

struct request_headers {
    enum method method;
    char *uri;
    char *query_string;
    char *file_extension;
    struct request_header* entries;
};

struct request_header {
    char *key;
    char *value;
    struct request_header* next;
};

struct request_headers* http_request_new();
int http_request_free(struct request_headers* header);
int http_request_parse(struct request_headers* header, char* buf, int len);
char* http_request_get(struct request_headers* header, char* key);
void http_request_debug(struct request_headers* header);
enum method http_request_get_method(struct request_headers* header);
int http_request_get_request(int sock, char* buf, int len);
#endif
